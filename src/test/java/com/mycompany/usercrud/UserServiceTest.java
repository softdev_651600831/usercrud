/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.usercrud;

import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author HP
 */
public class UserServiceTest {
    UserService userService;
    
    public UserServiceTest() {
    }

    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        userService = new UserService();
        user newAdmin = new user("admin","Administater", "pass@1234", 'M', 'A');
        user newUser1 = new user("user1","User 1", "pass@1234", 'M', 'U');
        user newUser2 = new user("user2","User 2", "pass@1234", 'M', 'U');
        user newUser3 = new user("user3","User 3", "pass@1234", 'M', 'U');
        user newUser4 = new user("user4","User 4", "pass@1234", 'M', 'U');
        userService.addUser(newAdmin);
        userService.addUser(newUser1);
        userService.addUser(newUser2);
        userService.addUser(newUser3);
        userService.addUser(newUser4);
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of addUser method, of class UserService.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        user newUser = new user("admin","Administater", "pass@1234", 'M', 'A');
        UserService instance = new UserService();
        user expResult = newUser;
        user result = instance.addUser(newUser);
        assertEquals(expResult, result);
        assertEquals(1, result.getId());
        user newUser2 = new user("user1","User 1", "pass@1234", 'M', 'U');
        user result2 = instance.addUser(newUser2);
        assertEquals(2, result2.getId());
        
    }

    /**
     * Test of getUser method, of class UserService.
     */
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        int indek = 1;
        
        
        String expResult = "user1";
        user result = userService.getUser(indek);
        assertEquals(expResult, result.getLogin());
    }

    /**
     * Test of getUsers method, of class UserService.
     */
    @Test
    public void testGetUsers() {
        System.out.println("getUsers");
       
        ArrayList<user> userList = userService.getUsers();
        assertEquals(5, userList.size());
        
    }

    /**
     * Test of getSize method, of class UserService.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        
        int expResult = 5;
        int result = userService.getSize();
        assertEquals(expResult, result);
       
    }
    
}
