/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usercrud;

/**
 *
 * @author HP
 */
public class user {
    private int Id;
    private String login;
    private String name;
    private String password;
    private char gender;
    private char role;
    
    public user(int Id, String login, String name, String password, char gender, char role) {
        this.Id = Id;
        this.login = login;
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public user(String login, String name, String password, char gender, char role) {
        this(-1, login, name, password, gender, role);
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public char getGender() {
        return gender;
    }
    
    public String getGenderString() {
        if (gender == 'M') {
            return "Male";
        } else {
            return "Female";
        }
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public char getRole() {
        return role;
    }
    
    public String getRoleString() {
        if (role == 'A') {
            return "Admin";
        } else {
            return "User";
        }
    }

    public void setRole(char role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "user{" + "Id=" + Id + ", login=" + login + ", name=" + name + ", password=" + password + ", gender=" + gender + ", role=" + role + '}';
    }
    
}
