/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usercrud;

import java.util.ArrayList;

/**
 *
 * @author HP
 */
public class UserService {
    private ArrayList<user> userList;
    private int lastId = 1;
    
    public UserService(){
        userList = new ArrayList<user>();  
    }
    
    public user addUser(user newUser){
        newUser.setId(lastId++);  
        userList.add(newUser);
        return newUser;
    }
    
    public user getUser(int indek){
        return userList.get(indek);
    }
    
    public ArrayList<user> getUsers(){
        return userList;
    }
    
    public int getSize(){
        return userList.size();
    }
    public void logUserList(){
        for (user u : userList) {
            System.out.println(u);
        }
    }

    user updateUser(int index, user updatedUser) {
        user User = userList.get(index);
        User.setLogin(updatedUser.getLogin());
        User.setName(updatedUser.getName());
        User.setPassword(updatedUser.getPassword());
        User.setGender(updatedUser.getGender());
        User.setRole(updatedUser.getRole());
        return User;
    }

    user deleteUser(int index) {  
        return userList.remove(index);
    }
}
